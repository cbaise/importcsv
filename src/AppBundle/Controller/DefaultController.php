<?php

namespace AppBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;

// instantiation, when using it as a component
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use Symfony\Component\Form\Extension\Core\Type\FileType;



class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $defaultData = array('message' => 'CSV');
        $form = $this->createFormBuilder($defaultData)
            ->add('attachment', FileType::class,[
                'label' => 'Select file'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $dir = $this->container->getParameter('kernel.root_dir').'/../web/temp/';

            $file = $form['attachment']->getData();

            $filename = 'matches.csv';

            $file->move($dir, $filename);

            $this->addFlash(
                'success',
                'File uploaded!'
            );

            return $this->redirectToRoute('done');
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
//            'datos' => $data
        ]);
    }


    /**
     * @Route("/done/", name="done")
     */
    public function showAction() {

        $path = $this->container->getParameter('kernel.root_dir').'/../web/temp/matches.csv';
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

//      decoding CSV contents
        $data = $serializer->decode(file_get_contents($path), 'csv');

        $fs = new Filesystem();
        $fs->remove($path);

        return $this->render('default/done.html.twig', [
            'data' => $data
        ]);
    }
}
